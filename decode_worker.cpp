#include "decode_worker.h"

#include <QThread>
#include <QVector>
//#include <QDebug>
#include <QTimer>
#include <QTime>
#include <QGLWidget>
#include <opencv2/imgproc/imgproc.hpp>

double DecodeWorker::posMSec() const
{
    QMutexLocker lock(&_mutex);
    return _posMSec;
}

double DecodeWorker::posFrames() const
{
    QMutexLocker lock(&_mutex);
    return _posFrames;
}

void DecodeWorker::setPosFrames(double frame)
{
    _mutex.lock();
    _stream.set(cv::CAP_PROP_POS_FRAMES, frame);
    _mutex.unlock();
    updateAllProperties();
    //qDebug() << "CurrentFrame:" << _posFrames << "requested frame: " << frame;
}

double DecodeWorker::posRelative() const
{
    QMutexLocker lock(&_mutex);
    return _posRelative;
}

double DecodeWorker::framerate() const
{
    QMutexLocker lock(&_mutex);
    return _framerate;
}

double DecodeWorker::codec() const
{
    QMutexLocker lock(&_mutex);
    return _codec;
}

double DecodeWorker::frameCount() const
{
    QMutexLocker lock(&_mutex);
    return _frameCount;
}

bool DecodeWorker::openStream(QString stream)
{
    return _stream.open(stream.toStdString());
}

void DecodeWorker::updateVolatileProperties()
{
    _posMSec = _stream.get(cv::CAP_PROP_POS_MSEC);
    _posFrames = _stream.get(cv::CAP_PROP_POS_FRAMES);
    _posRelative = _stream.get(cv::CAP_PROP_POS_AVI_RATIO);
}

void DecodeWorker::log(QString message)
{
    QFile file("gazelle.log");

    if (file.open(QIODevice::ReadWrite | QIODevice::Append)) {
        QTextStream stream(&file);
        stream << message << endl;
    }
}

void DecodeWorker::updateAllProperties()
{
    QMutexLocker lock(&_mutex);
    _posMSec = _stream.get(cv::CAP_PROP_POS_MSEC);
    _posFrames = _stream.get(cv::CAP_PROP_POS_FRAMES);
    _posRelative = _stream.get(cv::CAP_PROP_POS_AVI_RATIO);
    _framerate = _stream.get(cv::CAP_PROP_FPS);
    _codec = _stream.get(cv::CAP_PROP_FOURCC);
    _frameCount = _stream.get(cv::CAP_PROP_FRAME_COUNT);
}

static void matDeleter(void* mat)
{
    delete static_cast<cv::Mat*>(mat);
}
void DecodeWorker::decode(int frames)
{
    for (int i = 0; i < frames; i++) {

        _mutex.lock();
        QTime t;
        t.start();
        updateVolatileProperties();
        //Decode with Debug and log uncommented
        if (_posFrames < _frameCount) {
            //QString logMessage;
            QScopedPointer<cv::Mat> frame(new cv::Mat);
            //qDebug() << "update Properties:" << t.elapsed();
            //logMessage += QString::number(t.elapsed()) + "\t";
            //t.restart();
            _stream >> *frame;
            //qDebug() << "decode:" << t.elapsed()<<"ms";
            //logMessage += QString::number(t.elapsed()) + "\t";
            //t.restart();
            cv::cvtColor(*frame, *frame, CV_BGR2RGB);
            //qDebug() << "To rgb:" << t.elapsed()<<"ms";
            //logMessage += QString::number(t.elapsed()) + "\t";
            //t.restart();
            const QImage qimage(frame->data, frame->cols, frame->rows, frame->step,
                                QImage::Format_RGB888, matDeleter);
            //qDebug() << "toQImage:" << t.elapsed()<<"ms";
            //logMessage += QString::number(t.elapsed()) + "\t";
            //t.restart();
            QGraphicsPixmapItem* image = new QGraphicsPixmapItem();
            image->setPixmap(QPixmap::fromImage(qimage, Qt::ThresholdDither));
            //logMessage += QString::number(t.elapsed()) + "\t";
            //log(logMessage);
            //qDebug() << "Thread ID:" << QThread::currentThreadId();
            emit DecodeWorker::resultReady(image, _posFrames);
        } else {
            emit resultReady(NULL, -1);
        }

        _mutex.unlock();
    }
}

QImage DecodeWorker::cvMatToQImage(cv::Mat& inMat)
{

    switch (inMat.type()) {
    // 8-bit, 4 channel
    case CV_8UC4: {
        return QImage(inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB32, matDeleter);

    }

    // 8-bit, 3 channel
    case CV_8UC3: {
        return QImage(inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_RGB888, matDeleter);

    }

    // 8-bit, 1 channel
    case CV_8UC1: {
        // only create our color table once
        if (_colorTable.isEmpty()) {
            for (int i = 0; i < 256; ++i) {
                _colorTable.push_back(qRgb(i, i, i));
            }
        }

        QImage image(inMat.data, inMat.cols, inMat.rows, inMat.step, QImage::Format_Indexed8, matDeleter);
        image.setColorTable(_colorTable);
        return image;
    }

    default:
        //qWarning() << "ASM::cvMatToQImage() - cv::Mat image type not handled in switch:" << inMat.type();
        break;
    }

    return QImage();
}
