#ifndef VIDEO_HANDLER_H
#define VIDEO_HANDLER_H

#include "decode_worker.h"
#include "overlay.h"
#include <QImage>
#include <QMutex>
#include <QPixmap>
#include <QTime>
#include <QTimer>
#include <QThread>
#include <QObject>
#include <QHash>

using namespace cv;
/**
 * @brief The VideoHandler class handles when each frame and overlay gets displayed on screen synchronized,
 * handles the video buffer, requests frames as needed, allows controll of the playback
 */
class VideoHandler : public QObject
{
    Q_OBJECT
public:
    VideoHandler();
    ~VideoHandler();
    /**
     * @brief VideoHandler::posMSec
     * @return Current position of the playback in milliseconds
     */
    double posMSec() const;
    /**
     * @brief VideoHandler::posFrames
     * @return Current positon of the playback in frameposition
     */
    double posFrames() const;
    /**
     * @brief VideoHandler::setPosFrames
     * @param frame Frame to set the DecodeWorker to.
     * @param updateCurrentFrame Set
     */
    void setPosFrames(double frame, bool updateCurrentFrame=true);
    /**
     * @brief VideoHandler::posRelative
     * @return Current positon of the playback relative to the entire playback
     */
    double posRelative() const;
    /**
     * @brief VideoHandler::framerate
     * @return Framerate of the video that is currently decoded
     */
    double framerate() const;
    /**
     * @brief VideoHandler::codec
     * @return Codec of the video that is currently decoded
     */
    double codec() const;
    /**
     * @brief VideoHandler::frameCount
     * @return The total amount of frames in the current playback
     */
    double frameCount() const;
    /**
     * @brief The PlayState enum holds if state of the video playback
     */
    enum PlayState {pause, ///< Video and overlay is paused
                    playVideo, ///< Video is playing
                    playOverlays ///< Overlay is playing
                   };
    /**
     * @brief VideoHandler::playState returns the current playState of the video playback
     * @return
     */
    PlayState playState() const;
public slots:
    /**
     * @brief VideoHandler::play starts video playback with normal speed
     * If the video is already playing it pauses the playback
     */
    void play();
    /**
     * @brief VideoHandler::playOverlay starts the playback of the overlays with the
     * speed of the normal video.
     * If the overlays are already playing it will pause the playback
     */
    void playOverlay();
    /**
     * @brief VideoHandler::timeout slot that gets called if a new frame
     * or overlay needs to be displayed.
     * Reduces the buffer size to 50 Images if the buffer is bigger than 100 Images
     */
    void timeout();
    /**
     * @brief VideoHandler::open opens a new video file and deletes all previous buffered images
     */
    void open();
    /**
     * @brief VideoHandler::openOverlay opens a csv file and parses it.
     */
    void openOverlay();
    /**
     * @brief VideoHandler::openTimestamp Opens timestamp file and parses the file
     */
    void openTimestamp();
    /**
     * @brief VideoHandler::imageFreed slots that gets calles when _currentImage and
     * _previousImage are removed from the scene. And it deletes them
     */
    void imageFreed();
    /**
     * @brief VideoHandler::nextImage slot that sends the next image and updates the overlay
     */
    void nextImage();
    /**
     * @brief VideoHandler::nextOverlay slot that sends the next Overlay and updates the Image if needed
     */
    void nextOverlay();
    /**
     * @brief VideoHandler::previousImage slot that sends the previous Image and updates the overlay
     */
    void previousImage();
    /**
     * @brief VideoHandler::previousOverlay slot that sends the previous Overlay and updates the Image if needed
     */
    void previousOverlay();
signals:
    /**
     * @brief updateSlider emitted when the slider needs to be updated
     * @param totalFrames total count of frames
     */
    void updateSlider(int totalFrames);
    /**
     * @brief imageRefresh emitted when the image needs to be updated
     * @param image
     */
    void imageRefresh(QGraphicsPixmapItem* image);
    /**
     * @brief overlayRefresh emitted when the overlay needs to be adjusted
     * @param pos
     */
    void overlayRefresh(QPoint pos);
    /**
     * @brief decode order new frame(s) to be decoded and converted
     * @param images count of frames to be decoded
     */
    void decode(int images);
    /**
     * @brief freeImage request ownership of images that are displayed
     */
    void freeImage();
private:
    /**
     * @brief VideoHandler::sendNextImage sends the next Image to MainWindow and frees the buffer when it goes back
     * Requests new Images to be decoded if needed
     * @param forward if true the next displayed image is the next image of the stream, otherwise it is the previous
     *
     */
    void sendNextImage(bool forward);
    /**
     * @brief VideoHandler::checkOverlay synces the overlay to the current frame and updates
     * the current position of the frame
     */
    void checkOverlay();
    /*Pointer to the image where the ownership might be on the QGraphicscene*/
    QGraphicsPixmapItem* _currentImage;
    QGraphicsPixmapItem* _previousImage;
    QHash<int, QGraphicsPixmapItem*> _bufferedImages;
    DecodeWorker _decodeWorker;
    QTimer _timer;
    QTime _refTimer;
    Overlay _overlays;
    QMutex _mutex;
    QThread _t;
    PlayState  _playState;
    int _currentFrame = -1;
    qint64 _currentTimestamp=-1;
    bool _toDisplay=false;
    double _playSpeed;
    double _frameTime;
    int _smallBufferIndex;
    bool _imageReady;
private slots:
    /**
     * @brief VideoHandler::decodeReady slot that gets called when a frame is ready to be displayed
     * Stores the result in the buffer and displays the frame if that was previously requested
     * @param result the decoded frame
     * @param frame the framenumber of the decoded frame
     */
    void decodeReady(QGraphicsPixmapItem* image, int frame);
    /**
     * @brief VideoHandler::freeBuffer deletes all stored frames in the buffer outisede the keep parameters
     * @param keepStart All frames with a framenumber smaller than keepStart are deleted
     * @param keepStop All frames with a framenumber bigger than keepStop are deleted
     */
    void freeBuffer(int keepStart = -1, int keepStop = -1);
};

#endif // VIDEO_HANDLER_H
