#-------------------------------------------------
#
# Project created by QtCreator 2016-03-19T12:14:22
#
#-------------------------------------------------

QT       += core gui opengl

INCLUDEPATH += /usr/local/include/opencv
LIBS += -L/usr/local/lib -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_videoio -lopencv_video -lopencv_videostab -lopencv_imgproc

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = gazelle_view
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    overlay.cpp \
    decode_worker.cpp \
    video_handler.cpp

HEADERS  += mainwindow.h \
    overlay.h \
    decode_worker.h \
    video_handler.h

FORMS    += mainwindow.ui

RESOURCES += \
    icons.qrc

DISTFILES +=
