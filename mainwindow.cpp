#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent),
    _ui(new Ui::MainWindow)
{
    _ui->setupUi(this);


    //Set icons from icon theme, use fallback if theme doesn't return icons
    _ui->goNextBtn->setIcon(QIcon::fromTheme("go-next", QIcon(":/Images/arrows.png")));
    _ui->goPreviousBtn->setIcon(QIcon::fromTheme("go-previous", QIcon(":/Images/left-arrow.png")));
    _ui->PlayButton->setIcon(QIcon::fromTheme("media-playback-start", QIcon(":/Images/play-arrow.png")));
    _ui->goNextOverlayBtn->setIcon(QIcon::fromTheme("go-next", QIcon(":/Images/arrows.png")));
    _ui->goPreviousOverlayBtn->setIcon(QIcon::fromTheme("go-previous", QIcon(":/Images/left-arrow.png")));
    _ui->playOverlayButton->setIcon(QIcon::fromTheme("media-playback-start", QIcon(":/Images/play-arrow.png")));

    //instantiate objects
    _scene = new QGraphicsScene(this);
    _videoHandler = new VideoHandler();
    _currentImage = new QGraphicsPixmapItem();
    _overlay = new QGraphicsEllipseItem(QRectF(-10, -10, 20, 20));

    //Set properties
    _overlay->setBrush(QBrush(Qt::red));
    _overlay->setVisible(false);
    _overlay->setZValue(10);
    _ui->graphicsView->setScene(_scene);
    _scene->addItem(_currentImage);
    _scene->addItem(_overlay);
    _ui->graphicsView->setBackgroundBrush(QBrush(Qt::black));
    _ui->graphicsView->show();

    //connect signal and slots
    connect(_ui->openButton, &QPushButton::pressed,
            _videoHandler, &VideoHandler::open);
    connect(_videoHandler, &VideoHandler::updateSlider,
            this, &MainWindow::updateSlider);
    connect(_ui->PlayButton, &QPushButton::pressed,
            _videoHandler, &VideoHandler::play);
    connect(_ui->PlayButton, &QPushButton::pressed,
            this, &MainWindow::updateIcons);
    connect(_videoHandler, &VideoHandler::imageRefresh,
            this, &MainWindow::displayImage);
    connect(_videoHandler, &VideoHandler::freeImage,
            this, &MainWindow::freeImages);
    connect(this, &MainWindow::freedImages,
            _videoHandler, &VideoHandler::imageFreed);
    connect(_videoHandler, &VideoHandler::overlayRefresh,
            this, &MainWindow::displayOverlay);

    _refTimer.start();
}

MainWindow::~MainWindow()
{
    //_currentImage is deleted by videoHandler,
    //_scene is child of Mainwindow, so qt takes care of deleting that
    delete _ui;
    delete _videoHandler;
}

void MainWindow::updateIcons()
{
    switch (_videoHandler->playState()) {
    case VideoHandler::pause:
        _ui->PlayButton->setIcon(QIcon::fromTheme("media-playback-start", QIcon(":/Images/play-arrow.png")));
        _ui->playOverlayButton->setIcon(QIcon::fromTheme("media-playback-start", QIcon(":/Images/play-arrow.png")));
        break;

    case VideoHandler::playOverlays:
        _ui->PlayButton->setIcon(QIcon::fromTheme("media-playback-start", QIcon(":/Images/play-arrow.png")));
        _ui->playOverlayButton->setIcon(QIcon::fromTheme("media-playback-pause", QIcon(":/Images/pause.png")));
        break;

    case VideoHandler::playVideo:
        _ui->PlayButton->setIcon(QIcon::fromTheme("media-playback-pause", QIcon(":/Images/pause.png")));
        _ui->playOverlayButton->setIcon(QIcon::fromTheme("media-playback-start", QIcon(":/Images/play-arrow.png")));
        break;

    default:
        break;
    }
}

void MainWindow::displayImage(QGraphicsPixmapItem* image)
{
    _refTimer.restart();
    if (_currentImage != image) {
        _scene->addItem(image);
        _scene->removeItem(_currentImage);
        _currentImage = image;

        if (!_ui->positionSlider->isSliderDown()) {
            _ui->positionSlider->setValue(_videoHandler->posFrames());
        }
    }

    _ui->graphicsView->fitInView(_scene->sceneRect(), Qt::KeepAspectRatio);
    _ui->graphicsView->update();
}

void MainWindow::displayOverlay(QPoint pos)
{
    if (!pos.isNull()) {
        _overlay->setPos(pos);
    }
}

void MainWindow::updateSlider(int totalFrames)
{
    _ui->positionSlider->setMaximum(totalFrames);
}

void MainWindow::freeImages()
{
    _scene->removeItem(_currentImage);
    emit freedImages();
}

void MainWindow::on_goNextBtn_pressed()
{
    _videoHandler->nextImage();
}

void MainWindow::on_goPreviousBtn_pressed()
{
    _videoHandler->previousImage();
}

void MainWindow::on_positionSlider_sliderReleased()
{
    _videoHandler->setPosFrames(_ui->positionSlider->value());
}

void MainWindow::on_goNextOverlayBtn_pressed()
{
    _videoHandler->nextOverlay();
}

void MainWindow::on_openOverlay_pressed()
{
    _videoHandler->openOverlay();
}

void MainWindow::on_openOverlay_2_pressed()
{
    _videoHandler->openTimestamp();
}

void MainWindow::on_goPreviousOverlayBtn_pressed()
{
    _videoHandler->previousOverlay();
}

void MainWindow::on_playOverlayButton_pressed()
{
    _videoHandler->playOverlay();
    updateIcons();
}
void MainWindow::on_overlayVisible_stateChanged(int arg1)
{
    Q_UNUSED(arg1);
    _overlay->setVisible(_ui->overlayVisible->isChecked());
}
