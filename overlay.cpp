#include "overlay.h"
#include <QMap>
#include <QTime>
#include <QObject>
#include <QVector2D>
#include <QGraphicsItem>
#include <QFile>
#include <QTextStream>
Overlay::Overlay()
{
    //We don't need to allocate any resources
}

QPair<QVector2D, qint64> Overlay::nextOverlay(quint32 timeStamp)
{
    QMap<quint32, QVector2D>::const_iterator upperBound = _overlayItems.upperBound(timeStamp);

    if (upperBound != _overlayItems.cend()) {
        _currentTimestamp = upperBound.key();
        return qMakePair<QVector2D, qint64>(upperBound.value(), upperBound.key());
    } else {
        return qMakePair<QVector2D, qint64>(QVector2D(), -1);
    }
}

QPair<QVector2D, qint64> Overlay::nextOverlay()
{
    return nextOverlay(_currentTimestamp + 1);
}

QPair<QVector2D, qint64> Overlay::previousOverlay(quint32 timeStamp)
{
    QMap<quint32, QVector2D>::const_iterator lowerBound = _overlayItems.lowerBound(timeStamp);
    --lowerBound;

    if (lowerBound != _overlayItems.cbegin() - 1) {
        _currentTimestamp = lowerBound.key();
        return qMakePair<QVector2D, qint64>(lowerBound.value(), lowerBound.key());
    } else {
        return qMakePair<QVector2D, qint64>(QVector2D(), -1);
    }
}

QPair<QVector2D, qint64> Overlay::previousOverlay()
{
    return previousOverlay(_currentTimestamp - 1);
}

QVector<QVector2D> Overlay::overlaysToFrame(int frame)
{

    QMap<quint32, QVector2D>::const_iterator lowerBound = NULL;
    QMap<quint32, QVector2D>::const_iterator upperBound = NULL;
    QMap<quint32, QVector2D>::const_iterator end = _overlayItems.cend();

    if (frame < 0) {
        return QVector<QVector2D>();
    }

    if (_sceneFrames.size() > frame) {
        lowerBound = _overlayItems.upperBound(_sceneFrames.at(frame));
    } else {
        return QVector<QVector2D>();
    }

    if (_sceneFrames.size() > frame + 1) {
        upperBound = _overlayItems.upperBound(_sceneFrames.at(frame + 1));
    } else {
        upperBound = _overlayItems.cend();
    }

    QVector<QVector2D> overlays;

    if (frame < _sceneFrames.size()) {
        _currentTimestamp = _sceneFrames.at(frame);
    }

    while (lowerBound != upperBound && lowerBound != end) {
        overlays.append(lowerBound.value());
        lowerBound++;
    }

    return overlays;

}

Overlay::ParserError Overlay::parseOverlays(QString fileName)
{
    _overlayItems.clear();
    QFile file(fileName);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return OpenFileError;
    }

    QTextStream in(&file);

    while (!in.atEnd()) {
        QStringList list = in.readLine().split('\t', QString::SkipEmptyParts);
        bool valid = true;
        //ignore lines with too few tabs or lines beginning with '#'
        if (list.size() >= 3 && list.at(0).at(0) != '#') {
            qint64 timeStamp;
            QVector2D position;
            timeStamp = list.at(0).toLongLong(&valid);
            valid &=  timeStamp >= 0;

            if (valid) {
                position.setX(list.at(1).toFloat(&valid));
            }

            if (valid) {
                position.setY(list.at(2).toFloat(&valid));
            }

            if (!valid) {
                _overlayItems.clear();
                return ParseError;
            }

            _overlayItems.insert(_overlayItems.constEnd(), timeStamp, position);

        }
    }

    if (_overlayItems.size() > 0) {
        return NoError;
    } else {
        return ParseError;
    }
}

Overlay::ParserError Overlay::parseFrames(QString fileName)
{
    QFile file(fileName);
    _sceneFrames.clear();

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return OpenFileError;
    }

    QTextStream in(&file);

    while (!in.atEnd()) {
        QStringList list = in.readLine().split('\t', QString::SkipEmptyParts);
        bool valid = true;

        //ignore empty lines or lines beginning with '#'
        if (list.size() >= 1 && list.at(0).at(0) != '#') {
            quint32 timeStamp;
            timeStamp = list.at(0).toUInt(&valid);

            if (!valid)  {
                _sceneFrames.clear();
                return ParseError;
            }

            _sceneFrames.append(timeStamp);
        }

    }

    if (_sceneFrames.size() > 0) {
        return NoError;
    } else {
        return ParseError;
    }
}

qint64 Overlay::timeStamp(int frame)
{
    if (0 <= frame && frame < _sceneFrames.size()) {
        return _sceneFrames.at(frame);
    } else {
        return -1;
    }
}

int Overlay::frame(quint32 timestamp)
{
    int size = 1;

    //pow(2,floor(log(2, _sceneFrames.size())+1) without math.h
    //size will be the next 2^x that is greater than frameCount
    int frameCount = _sceneFrames.size();

    while (size < frameCount) {
        size *= 2;
    }

    int currentFrame = 0;

    //_sceneFrames are sorted, so it is possible to find it in logarithmic time
    for (int i = size / 2; i >= 1; i /= 2) {
        if ((frameCount > (currentFrame + i)) &&
                (_sceneFrames.at(currentFrame + i) < timestamp)) {
            currentFrame += i;
        }
    }

    return currentFrame;
}
