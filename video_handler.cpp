#include "video_handler.h"
//#include <QDebug>
#include <QFileDialog>
#include <QStandardPaths>
#include <iostream>



VideoHandler::VideoHandler() : _mutex(QMutex::Recursive)
{
    _timer.setTimerType(Qt::PreciseTimer);
    connect(&_timer, &QTimer::timeout, this, &VideoHandler::timeout);
    connect(&_decodeWorker, &DecodeWorker::resultReady, this, &VideoHandler::decodeReady);
    connect(this, &VideoHandler::decode, &_decodeWorker, &DecodeWorker::decode);
    _decodeWorker.moveToThread(&_t);
    _t.start();
    _t.setPriority(QThread::HighestPriority);


    _currentImage = NULL;
    _playState = pause;

}

VideoHandler::~VideoHandler()
{
    freeBuffer();
}

double VideoHandler::posMSec() const
{
    return _decodeWorker.posMSec();
}

double VideoHandler::posFrames() const
{
    return _decodeWorker.posFrames();
}

void VideoHandler::setPosFrames(double frame, bool updateCurrentFrame)
{
    _decodeWorker.setPosFrames(frame);

    if (updateCurrentFrame) {
        _currentFrame = static_cast<int>(_decodeWorker.posFrames());
        _toDisplay = true;
    }

    freeBuffer(frame - 25, frame + 10);
    emit decode(2);
}

double VideoHandler::posRelative() const
{
    return _decodeWorker.posRelative();
}

double VideoHandler::framerate() const
{
    return _decodeWorker.framerate();
}

double VideoHandler::codec() const
{
    return _decodeWorker.codec();
}

double VideoHandler::frameCount() const
{
    return _decodeWorker.frameCount();
}

void VideoHandler::play()
{
    switch (_playState) {
    case pause:
        _frameTime = 1000 / _decodeWorker.framerate()  + 0.5;
        _timer.setInterval(_frameTime);
        _timer.start();
        _refTimer.start();
        emit decode(3);
        _imageReady = false;
        _playState = playVideo;
        break;

    case playVideo:
        _timer.stop();
        _playState = pause;
        break;

    case playOverlays:
        _playState = playVideo;
        break;
    }
}

void VideoHandler::playOverlay()
{
    switch (_playState) {
    case pause:
        play();
        _playState = playOverlays;
        break;

    case playVideo:
        _playState = playOverlays;
        break;

    case playOverlays:
        _timer.stop();
        _playState = pause;
        break;
    }
}

void VideoHandler::timeout()
{
    //qDebug()<<"timout: " << _refTimer.elapsed();
    _refTimer.restart();

    if (!_mutex.tryLock()) {
        return;
    }

    switch (_playState) {
    case pause:
        break;

    case playVideo:
        if (_currentFrame < frameCount()-1) {
            sendNextImage(true);
            checkOverlay();

            if (_bufferedImages.size() > 100) {
                freeBuffer(_currentFrame - 50, frameCount());
            }
        } else {
            setPosFrames(0,true);
        }

        break;

    case playOverlays:
        nextOverlay();
        break;
    }


    _mutex.unlock();
}

void VideoHandler::open()
{
    QString fileName = QFileDialog::getOpenFileName(NULL,
                       tr("Open Video File"),
                       QStandardPaths::displayName(QStandardPaths::MoviesLocation) ,
                       tr("Video Files (*.mkv *.avi *.mp4)"));

    if (fileName.length() > 0) {

        _decodeWorker.openStream(fileName);
        _decodeWorker.updateAllProperties();
        freeBuffer();
        emit freeImage();
        emit updateSlider(_decodeWorker.frameCount());
    }
}

void VideoHandler::openOverlay()
{
    QString fileName = QFileDialog::getOpenFileName(NULL,
                       tr("Open Overlay File"),
                       QStandardPaths::displayName(QStandardPaths::MoviesLocation) ,
                       tr("Coma separated Values (*.csv)"));

    if (fileName.length() > 0) {
        switch (_overlays.parseOverlays(fileName)) {
        case Overlay::NoError:
            std::cout << "Parsing Overlays succesfull";
            break;

        case Overlay::OpenFileError:
            std::cout << "Can't open file";
            break;

        case Overlay::ParseError:
            std::cout << "Parse Error";
            break;

        }

    }
}

void VideoHandler::openTimestamp()
{
    QString fileName = QFileDialog::getOpenFileName(NULL,
                       tr("Open Timestamp File"),
                       QStandardPaths::displayName(QStandardPaths::MoviesLocation) ,
                       tr("Coma separated Values (*.csv)"));

    if (fileName.length() > 0) {
        switch (_overlays.parseFrames(fileName)) {
        case Overlay::NoError:
            std::cout << "Parsing Timestamps succesfull";
            break;

        case Overlay::OpenFileError:
            std::cout << "Can't open file";
            break;

        case Overlay::ParseError:
            std::cout << "Parse Error";
            break;

        }

    }
}

void VideoHandler::imageFreed()
{
    if (_currentImage == NULL) {
        delete _currentImage;
        _currentImage = NULL;
    }

    if (_previousImage == NULL) {
        delete _previousImage;
        _previousImage = NULL;
    }
}

void VideoHandler::nextImage()
{
    sendNextImage(true);
    checkOverlay();
}

void VideoHandler::nextOverlay()
{
    QPair<QVector2D, qint64> nextOverlay = _overlays.nextOverlay(_currentTimestamp);
    //check if Overlay is valid
    if (nextOverlay.second != -1) {
        emit overlayRefresh(nextOverlay.first.toPoint());
        _currentTimestamp = nextOverlay.second;

        if (_overlays.frame(nextOverlay.second) > _currentFrame) {
            sendNextImage(true);
        }
    }
}

void VideoHandler::previousImage()
{
    sendNextImage(false);

    checkOverlay();
}

void VideoHandler::previousOverlay()
{
    QPair<QVector2D, int> previousOverlay = _overlays.previousOverlay(_currentTimestamp);

    if (previousOverlay.second != -1) {
        emit overlayRefresh(previousOverlay.first.toPoint());
        _currentTimestamp = previousOverlay.second;

        if (_overlays.frame(previousOverlay.second) < _currentFrame) {
            sendNextImage(false);
        }
    }
}

void VideoHandler::sendNextImage(bool forward)
{
    if (!_mutex.tryLock()) {

        return;
    }

    if (forward) {
        _currentFrame++;
    } else {
        _currentFrame--;

        if (!_bufferedImages.contains(_currentFrame)) {
            setPosFrames(_currentFrame - 2, false);
            //emit decode(8);

            if (_bufferedImages.size() > 100) {
                freeBuffer(_currentFrame - 50, _currentFrame + 5);
            }

            _toDisplay = true;
        }
    }

    if (_bufferedImages.contains(_currentFrame)) {
        _previousImage = _currentImage;
        _currentImage = _bufferedImages[_currentFrame];

        emit imageRefresh(_currentImage);

        if (forward && !_bufferedImages.contains(_currentFrame + 1)) {
            emit decode(1);
        }

        if (forward && !_bufferedImages.contains(_currentFrame + 2)) {
            emit decode(1);
        }

        if (forward && !_bufferedImages.contains(_currentFrame + 3)) {
            emit decode(1);
        }

        if (forward) {
            _toDisplay = false;
        }
    } else if (forward) {
        emit decode(1);
        std::cout << "Frame Missed: " << _currentFrame;
        _currentFrame--;
    }

    _mutex.unlock();
}

void VideoHandler::checkOverlay()
{
    _currentTimestamp = _overlays.timeStamp(_currentFrame);

    if (_currentTimestamp != -1) {
        QPair<QVector2D, qint64> nextOverlay = _overlays.nextOverlay(_currentTimestamp);

        if (nextOverlay.second != -1) {
            _currentTimestamp = nextOverlay.second;
            emit overlayRefresh(nextOverlay.first.toPoint());
        }
    }
}

VideoHandler::PlayState VideoHandler::playState() const
{
    return _playState;
}

void VideoHandler::decodeReady(QGraphicsPixmapItem* result, int frame)
{
    //qDebug() << "Image" << frame << "ready" << "Thread ID:" << QThread::currentThreadId();;
    //insert the frame only when it is valid and not already exists
    if (frame != -1 && (!_bufferedImages.contains(frame))) {
        _bufferedImages.insert(frame, result);

        if (_toDisplay && frame == _currentFrame) {
            _currentImage = result;
            emit imageRefresh(result);
            _toDisplay = false;
            QVector<QVector2D> frameOverlays = _overlays.overlaysToFrame(_currentFrame);

            if (frameOverlays.size() > 0) {
                emit overlayRefresh(frameOverlays.at(0).toPoint());
            } else {
                emit overlayRefresh(QPoint());
            }
        }
    } else if (result != NULL) {
        //if the result is not used we need to delete it to prevent a memory leak
        delete result;
    }
}

void VideoHandler::freeBuffer(int keepStart, int keepStop)
{
    QVector<int> toDelete;
    QHash<int, QGraphicsPixmapItem*>::const_iterator cit = _bufferedImages.constBegin();
    QHash<int, QGraphicsPixmapItem*>::const_iterator cend = _bufferedImages.constEnd();

    //iterate over all elements of the buffer and remember the ones that need to be deleted
    //This is done because deleting an element changes the hash table and invalidates the iterator
    while (cit != cend) {
        if ((keepStart > cit.key() || cit.key() > keepStop) &&
                cit.value() != _currentImage &&
                cit.value() != _previousImage &&
                cit.value() != NULL) {
            toDelete.append(cit.key());
        }

        cit++;
    }

    //delete all Frames and removethem from the buffer
    for (int i = 0; i < toDelete.size(); i++) {
        delete _bufferedImages[toDelete.at(i)];
        _bufferedImages.remove(toDelete.at(i));
    }
}
