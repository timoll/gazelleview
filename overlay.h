#ifndef OVERLAY_H
#define OVERLAY_H
#include <QMap>
#include <QVector2D>
#include <QGraphicsItem>
/**
 * @brief The Overlay class parses Overlays and Timestamps and gives access to them
 */
class Overlay : public QObject
{

public:
    ///@brief Overlay::ParseError Enum for storing euccess of parsing
    enum ParserError {NoError, ///< Parsing was successfull
                      OpenFileError, ///< Could not open File to parse
                      ParseError ///< An Error occured while parsing
                     };

    Overlay();
    /**
     * @brief Overlay::nextOverlay gets the next overlay after timeStamp
     * @param timeStamp
     * @return position and timestamp of the next overlay after timeStamp
     * If there is no next Overlay after timeStamp the function returns -1 as timeStamp
     */
    QPair<QVector2D, qint64> nextOverlay(quint32 timeStamp);
    /**
     * @brief Overlay::nextOverlay gets the next overlay after a timeStamp that is internaly stored
     * @return position and timestamp of the next overlay after timeStamp
     * If there is no next Overlay after an internal stored timestamp the function returns -1 as timeStamp
     */
    QPair<QVector2D, qint64> nextOverlay();
    /**
     * @brief Overlay::previousOverlay gets the previous overlay before timeStamp
     * @param timeStamp
     * @return position and timestamp of the previous overlay before timeStamp
     * If there is no previous Overlay before timeStamp the function returns -1 as timeStamp
     */
    QPair<QVector2D, qint64> previousOverlay(quint32 timeStamp);
    /**
     * @brief Overlay::previousOverlay gets the previous overlay before a timeStamp that is internaly stored
     * @return position and timestamp of the previous overlay before timeStamp
     * If there is no previous Overlay before an internal stored timestamp the function returns -1 as timeStamp
     */
    QPair<QVector2D, qint64> previousOverlay();
    /**
     * @brief Overlay::overlaysToFrame
     * @param frame framenumber
     * @return All overlays that have a timestamp between the timestamp of frame and the next frame as a QVector
     */
    QVector<QVector2D> overlaysToFrame(int frame);
    /**
     * @brief Overlay::parseOverlays parses the content of fileName as overlays.
     * Each line that has more or equal to 3 tab separated values and doesn start with '#' is parsed
     * @param fileName Path to the file that is to be parsed
     * @return ParserError OpenFileError when it isn't possible to open the file
     * ParseError when the parser failed to parse any line or a line contained invalid characters
     * Noerror when the parser could parse the file successfuly
     */
    Overlay::ParserError parseOverlays(QString fileName);
    /**
     * @brief Overlay::parseFrames parses the content of fileName as timestamp.
     * Each line that has more or equal to 1 tab separated values and doesn start with '#' is parsed
     * @param fileName Path to the file that is to be parsed
     * @return ParserError OpenFileError when it isn't possible to open the file
     * ParseError when the parser failed to parse any line or a line contained invalid characters
     * Noerror when the parser could parse the file successfuly
     */
    Overlay::ParserError parseFrames(QString fileName);
    /**
     * @brief Overlay::timeStamp  returns the timestamp to frame in constant time
     * @param frame
     * @return timestamp of frame with framenumber frame, -1 if frame is not available
     */
    qint64 timeStamp(int frame);
    /**
     * @brief Overlay::frame  returns the next lower framenumber that comes before timestamp
     * This is done in logarithmic time
     * @param timestamp used as a threshold
     * @return framenumber, always between 0 and the amount of parsed Frames
     */
    int frame(quint32 timestamp);

private:

    QMap<quint32, QVector2D> _overlayItems;
    QVector<quint32> _sceneFrames;
    quint32 _currentTimestamp;
};

#endif // OVERLAY_H
