#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "video_handler.h"

#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QMainWindow>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();
signals:
    /**
     * @brief freedImages The scene no longer holds ownership of any image
     */
    void freedImages();
private:

    Ui::MainWindow* _ui;
    VideoHandler* _videoHandler;
    QGraphicsScene* _scene;
    QGraphicsPixmapItem* _currentImage;
    QGraphicsEllipseItem* _overlay;
    QTime _refTimer;
private slots:
    /**
     * @brief MainWindow::updateIcons updates the play icons according to the playState of videoHandler
     */
    void updateIcons();
    /**
     * @brief MainWindow::displayImage Displays image and adjusts the slider
     * @param image
     */
    void displayImage(QGraphicsPixmapItem *image);
    /**
     * @brief displayOverlay Displays overlay
     * @param pos Where the overlay is placed
     */
    void displayOverlay(QPoint pos);
    /**
     * @brief updateSlider Update the position of the slider
     * @param totalFrames How many frames are there in total
     */
    void updateSlider(int totalFrames);
    /**
     * @brief MainWindow::freeImages take ownership of the frames away from the scene
     */
    void freeImages();
    void on_goNextBtn_pressed();
    void on_goPreviousBtn_pressed();
    void on_positionSlider_sliderReleased();
    void on_goNextOverlayBtn_pressed();
    void on_openOverlay_pressed();
    void on_openOverlay_2_pressed();
    void on_goPreviousOverlayBtn_pressed();
    void on_playOverlayButton_pressed();
    void on_overlayVisible_stateChanged(int arg1);
};

#endif // MAINWINDOW_H
