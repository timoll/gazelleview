\contentsline {chapter}{\numberline {1}Hierarchical Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Class Hierarchy}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Class Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Class List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Documentation}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Decode\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Worker Class Reference}{5}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Member Function Documentation}{5}{subsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.1}decode}{5}{subsubsection.3.1.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.2}open\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Stream(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}String stream)}{6}{subsubsection.3.1.1.2}
\contentsline {subsubsection}{\numberline {3.1.1.3}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Pos\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Frames(double frame)}{6}{subsubsection.3.1.1.3}
\contentsline {section}{\numberline {3.2}Main\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Window Class Reference}{6}{section.3.2}
\contentsline {section}{\numberline {3.3}Overlay Class Reference}{7}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Detailed Description}{8}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Member Enumeration Documentation}{8}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}Parser\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Error}{8}{subsubsection.3.3.2.1}
\contentsline {subsection}{\numberline {3.3.3}Member Function Documentation}{8}{subsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.1}frame(quint32 timestamp)}{8}{subsubsection.3.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.3.2}next\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Overlay(quint32 time\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Stamp)}{8}{subsubsection.3.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3.3}next\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Overlay()}{8}{subsubsection.3.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.4}overlays\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}To\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Frame(int frame)}{9}{subsubsection.3.3.3.4}
\contentsline {subsubsection}{\numberline {3.3.3.5}parse\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Frames(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}String file\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Name)}{9}{subsubsection.3.3.3.5}
\contentsline {subsubsection}{\numberline {3.3.3.6}parse\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Overlays(\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Q\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}String file\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Name)}{9}{subsubsection.3.3.3.6}
\contentsline {subsubsection}{\numberline {3.3.3.7}previous\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Overlay(quint32 time\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Stamp)}{10}{subsubsection.3.3.3.7}
\contentsline {subsubsection}{\numberline {3.3.3.8}previous\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Overlay()}{10}{subsubsection.3.3.3.8}
\contentsline {subsubsection}{\numberline {3.3.3.9}time\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Stamp(int frame)}{10}{subsubsection.3.3.3.9}
\contentsline {section}{\numberline {3.4}Video\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Handler Class Reference}{10}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Detailed Description}{12}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Member Enumeration Documentation}{12}{subsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.2.1}Play\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}State}{12}{subsubsection.3.4.2.1}
\contentsline {subsection}{\numberline {3.4.3}Member Function Documentation}{12}{subsection.3.4.3}
\contentsline {subsubsection}{\numberline {3.4.3.1}codec() const }{12}{subsubsection.3.4.3.1}
\contentsline {subsubsection}{\numberline {3.4.3.2}decode}{12}{subsubsection.3.4.3.2}
\contentsline {subsubsection}{\numberline {3.4.3.3}frame\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Count() const }{13}{subsubsection.3.4.3.3}
\contentsline {subsubsection}{\numberline {3.4.3.4}framerate() const }{13}{subsubsection.3.4.3.4}
\contentsline {subsubsection}{\numberline {3.4.3.5}image\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Refresh}{13}{subsubsection.3.4.3.5}
\contentsline {subsubsection}{\numberline {3.4.3.6}overlay\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Refresh}{13}{subsubsection.3.4.3.6}
\contentsline {subsubsection}{\numberline {3.4.3.7}play\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}State() const }{13}{subsubsection.3.4.3.7}
\contentsline {subsubsection}{\numberline {3.4.3.8}pos\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Frames() const }{14}{subsubsection.3.4.3.8}
\contentsline {subsubsection}{\numberline {3.4.3.9}pos\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Sec() const }{14}{subsubsection.3.4.3.9}
\contentsline {subsubsection}{\numberline {3.4.3.10}pos\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Relative() const }{14}{subsubsection.3.4.3.10}
\contentsline {subsubsection}{\numberline {3.4.3.11}set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Pos\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Frames(double frame, bool update\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Current\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Frame=true)}{14}{subsubsection.3.4.3.11}
\contentsline {subsubsection}{\numberline {3.4.3.12}update\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Slider}{14}{subsubsection.3.4.3.12}
\contentsline {chapter}{Index}{15}{section*.17}
