#ifndef DECODE_WORKER_H
#define DECODE_WORKER_H

#include <QFile>
#include <QMutex>
#include <QObject>
#include <QTextStream>
#include <QVector>

#include <QGraphicsPixmapItem>

#include <opencv2/videoio.hpp>
Q_DECLARE_METATYPE(cv::Mat)


using namespace cv;
class DecodeWorker : public QObject
{
    Q_OBJECT
public:
    double posMSec() const;
    double posFrames() const;
    /**
     * @brief DecodeWorker::setPosFrames sets the Current Frame of the decoder to frame
     * @param frame
     */
    void setPosFrames(double frame);
    double posRelative() const;
    double framerate() const;
    double codec() const;
    double frameCount() const;
    /**
     * @brief DecodeWorker::openStream opens stream
     * @param stream filename of the stream
     * @return true when successfull
     */
    bool openStream(QString stream);

    /**
     * @brief DecodeWorker::updateAllProperties updated every property that can be accessed
     */
    void updateAllProperties();

private:
    /**
     * @brief DecodeWorker::updateVolatileProperties update all properties that can change with the same stream
     */
    void updateVolatileProperties();
    void log(QString message);
    VideoCapture _stream;
    QScopedPointer<cv::VideoCapture> _videoCapture;
    mutable QMutex _mutex;
    double _posMSec;
    double _posFrames;
    double _posRelative;
    double _framerate;
    double _codec;
    double _frameCount;
    QVector<QRgb>  _colorTable;
    QFile _logFile;
    QTextStream _logStream;
    /**
     * @brief DecodeWorker::cvMatToQImage converts a cv::mat into a qImage
     * @param inMat cv::mat that will be converted
     * @return converted QImage
     */
    QImage cvMatToQImage(Mat& inMat);
signals:
    void resultReady(QGraphicsPixmapItem* image, int frame);
public slots:
    /**
     * @brief DecodeWorker::decode slot that decodes frames and converts them into a QGraphicsPixmapItem
     * @param frames
     */
    void decode(int frames);

};

#endif // DECODEWORKER_H
